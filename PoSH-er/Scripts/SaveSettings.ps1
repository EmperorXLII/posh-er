$ErrorActionPreference = 'Stop'

Write-Host
Write-Host 'Saving settings...'
$targets = @{
    'MainWindow' = @('Left','Top','Width','Height','WindowState','ShowSettings','ActionsRowHeight','ShowLoading')
    'SettingsView' = @('AutoSave','ShowSettingsOutput')
}
$settings = @{}

Write-Host ' - Saving' $targets.Count 'targets...'
foreach( $key in $targets.Keys ) {
    Write-Host '   - Saving' $key
    $target = Get-Variable $key -ValueOnly
    $targetSettings = $settings[$key] = @{}
    foreach( $property in $targets[$key] ) {
        $value = $target.$property
        Write-Host '     -' $property '=' $value
        if( $value -ne $null ) {
            $targetSettings[$property] = $value
        }
    }
}

$settings | ConvertTo-Json | Set-Content -Path .\Settings.json -Encoding UTF8

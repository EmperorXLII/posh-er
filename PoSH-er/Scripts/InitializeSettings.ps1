$ErrorActionPreference = 'Stop'

Write-Host
Write-Host 'Initializing settings directory...'
if( -not (Test-Path .\Settings.json) ) {
    Write-Host ' - Creating Settings.json file'
    Set-Content -Path ./Settings.json -Encoding UTF8 -Value '{ "MainWindow": { "ShowLoading": false } }'
}

if( -not (Test-Path .\Actions) ) {
    Write-Host ' - Creating Actions directory'
    New-Item -Path .\Actions -ItemType Directory
}

try {
    Write-Host ' - Initalizing Actions repository'
    Push-Location .\Actions
    git init .

    if( -not (Get-ChildItem . -Filter *.ps1 ) ) {
        Set-Content -LiteralPath '[_0].ps1' -Encoding UTF8 -Value ''
    }

    git add --all
    git commit --all --message="Initialized Actions directory."
}
finally {
    Pop-Location
}

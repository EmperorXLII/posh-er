$ErrorActionPreference = 'Stop'

Write-Host
Write-Host 'Loading actions...'
$actions = @(Get-ChildItem -Path .\Actions -Filter *.ps1 | Sort { $_.Name.Replace( '_', '' ) })

$oldIndexActions = @($actions | ? { [uint16]::TryParse( $_.BaseName, [ref]$null ) })
if( $oldIndexActions -and $oldIndexActions.Count -eq $actions.Count ) {
    Write-Host ' - Updating v1.0 action names' -NoNewline
    foreach( $action in $actions ) {
        Write-Host '.' -NoNewline
        $index = $action.BaseName
        $newIndexName = '[{0}].ps1' -f $index.PadLeft( 2, '_' )
        Rename-Item -LiteralPath $action.FullName -NewName $newIndexName
    }

    Write-Host
    $actions = @(Get-ChildItem -Path .\Actions -Filter *.ps1)
}

Write-Host ' - Adding' $actions.Count 'actions' -NoNewline
foreach( $action in $actions ) {
    Write-Host '.' -NoNewline
    $script = @(Get-Content -LiteralPath $action.FullName) -join [environment]::NewLine
    $MainWindow.AddAction( $action.BaseName, $script.TrimEnd( ) )
}

$ErrorActionPreference = 'Stop'

function Test-GitRepository( [string]$path ) {
    try {
        Push-Location $path
        if( git status --short ) { return $true }
    }
    catch {
        return $false
    }
    finally {
        Pop-Location
    }
}

Write-Host
Write-Host 'Testing initialization...'
if( Test-GitRepository $pwd ) {
    Write-Host ' - Uninitializing v1.0 settings...'
    Remove-Item -LiteralPath '.git' -Recurse -Force
    Remove-Item .\Settings\ -Recurse -Force
}

if( -not (Test-GitRepository .\Actions) ) {
    &$PSScriptRoot\InitializeSettings.ps1
}


function ConvertTo-Hashtable( $json ) {
    $result = @{}
    $keys = $json | Get-Member -MemberType NoteProperty | % Name
    foreach( $key in $keys ) {
        $result[$key] = $json.$key
    }
    $result
}

Write-Host
Write-Host 'Loading settings...'
$settingsJson = Get-Content -Path .\Settings.json -ErrorAction Continue | ConvertFrom-Json
$settings = ConvertTo-Hashtable $settingsJson
foreach( $target in @($settings.Keys) ) { $settings[$target] = ConvertTo-Hashtable $settings[$target] }

function SettingsTargetChange( [string]$change ) {
    Write-Host -ForegroundColor Gray " ~ Migrating $property from $target to $change"
    if( $settings[$change] -eq $null ) { $settings[$change] = @{} }
    $settings[$change][$property] = $value
    $settings[$target].Remove( $property )
}

$targetSettingsView = { SettingsTargetChange 'SettingsView' }
$migrations = @{ 'MainWindow' = @{ 'AutoSave' = $targetSettingsView; 'ShowSettingsOutput' = $targetSettingsView } }
foreach( $target in $migrations.Keys ) {
    $targetSettings = $settings[$target]
    $targetMigrations = $migrations[$target]
    foreach( $property in $targetMigrations.Keys ) {
        if( $targetSettings.ContainsKey( $property ) ) {
            $migration = $targetMigrations[$property]
            $value = $targetSettings[$property]
            & $migration
        }
    }
}

Write-Host ' - Updating' $settings.Count 'targets...'
foreach( $key in $settings.Keys ) {
    Write-Host '   - Updating' $key
    $target = Get-Variable $key -ValueOnly
    $targetSettings = $settings[$key]
    foreach( $property in $targetSettings.Keys ) {
        $value = $targetSettings[$property]
        Write-Host '     -' $property '=' $value
        if( $value -ne $null ) {
            $target.$property = $value
        }
    }
}

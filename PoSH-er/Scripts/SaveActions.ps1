$ErrorActionPreference = 'Stop'

Write-Host
Write-Host 'Retrieving old actions' -NoNewline
$old = @{}
$oldFiles = Get-ChildItem -Path .\Actions -Filter *.ps1
foreach( $file in $oldFiles ) {
    Write-Host '.' -NoNewline
    $old[$file.BaseName] = $file
}

$actions = @($MainWindow.Actions)
Write-Host
Write-Host 'Saving' $actions.Count 'actions' -NoNewline
foreach( $action in $MainWindow.Actions ) {
    Write-Host '.' -NoNewline
    [string]$name = $action.ActionName
    $old.Remove( $name )
    Set-Content -LiteralPath ".\Actions\$name.ps1" -Value $action.Script -Encoding UTF8
}

if( $old.Count ) {
    Write-Host
    Write-Host 'Deleting' $old.Count 'old actions' -NoNewline
    foreach( $file in $old.Values ) {
        Write-Host '.' -NoNewline
        $file.Delete( )
    }
}

try {
    Push-Location .\Actions
    git commit --all --message="Updated actions."
}
finally {
    Pop-Location
}

﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace EmperialApps.PoSHer {

    public partial class App : Application {
        private Settings _settings;

        public App( ) {
            this.DispatcherUnhandledException += ( s, e ) => this.ShowException( e.Exception );
        }

        public static new App Current => (App)Application.Current;

        private void ShowException( Exception exception ) => ShowException( this.MainWindow, exception );

        public static void ShowException( Window owner, Exception exception ) {
            var aggregate = exception as AggregateException;
            if( aggregate?.InnerExceptions.Count == 1 ) {
                ShowException( owner, aggregate.InnerException );
                return;
            }

            var lines = aggregate?.InnerExceptions.Select( ex => "- " + ex.Message ) ?? new[] { exception.Message };
            string message = string.Join( Environment.NewLine, lines );
            MessageBox.Show( owner, message, exception.GetType( ).ToString( ) );
        }

        protected override void OnActivated( EventArgs e ) {
            if( this._settings == null ) {
                var mainWindow = (MainWindow)this.MainWindow;
                this._settings = new Settings( mainWindow );
                this.Dispatcher.BeginInvoke( new Func<Task>( this.LoadAsync ) );

                mainWindow.Ran += this.AutoSaveActionsAsync;
            }

            base.OnActivated( e );
        }

        protected override void OnDeactivated( EventArgs e ) {
            if( this._settings != null ) {
                this.Dispatcher.BeginInvoke( new Func<Task>( this.SaveSettingsAsync ) );
            }

            base.OnDeactivated( e );
        }

        private async Task LoadAsync( ) {
            await this.Try( this._settings.LoadAsync );
            await this.Try( this._settings.LoadActionsAsync );
        }

        private Task SaveSettingsAsync( ) => this.Try( this._settings.SaveAsync );

        private async void AutoSaveActionsAsync( object sender, EventArgs e ) => await this.Try( this._settings.AutoSaveActionsAsync );

        public Task SaveActionsAsync( ) => this.Try( this._settings.SaveActionsAsync );

        private async Task Try( Func<Task> actionAsync ) {
            try {
                await actionAsync( );
            }
            catch( Exception ex ) {
                this.ShowException( ex );
            }
        }
    }
}

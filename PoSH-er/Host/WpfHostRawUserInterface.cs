﻿using System;
using System.Management.Automation;
using System.Management.Automation.Host;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using EmperialApps.PoSHer.Views;

using Size = System.Management.Automation.Host.Size;

namespace EmperialApps.PoSHer.Host {

    public class WpfHostRawUserInterface : PSHostRawUserInterface {
        private volatile ScriptView _view;

        public ScriptView View {
            get => this._view;
            set => this._view = value;
        }

        public override ConsoleColor ForegroundColor { get; set; } = ConsoleColor.DarkYellow;
        public override ConsoleColor BackgroundColor { get; set; } = ConsoleColor.DarkMagenta;

        public override Size BufferSize {
            get => new Size( 999, 999 );
            set => throw new NotImplementedException( );
        }

        public override Coordinates CursorPosition {
            get => throw new NotImplementedException( );
            set => throw new NotImplementedException( );
        }

        public override int CursorSize {
            get => throw new NotImplementedException( );
            set => throw new NotImplementedException( );
        }

        public override bool KeyAvailable => throw new NotImplementedException( );

        public override Size MaxPhysicalWindowSize => throw new NotImplementedException( );

        public override Size MaxWindowSize => throw new NotImplementedException( );

        public override Coordinates WindowPosition {
            get => throw new NotImplementedException( );
            set => throw new NotImplementedException( );
        }

        public override Size WindowSize {
            get => this.BufferSize;
            set => throw new NotImplementedException( );
        }

        public override string WindowTitle {
            get => AccessMainWindowAsync( w => w.Title ).Result;
            set => UpdateMainWindow( w => w.Title = value.Replace( nameof( PowerShell ), "PoSH-er" ) );
        }

        private static Window GetMainWindow( ) => Application.Current.MainWindow;

        private static async Task<T> AccessMainWindowAsync<T>( Func<Window, T> action ) {
            T result;

            var dispatcher = Application.Current.Dispatcher;
            if( dispatcher.CheckAccess( ) )
                result = action( GetMainWindow( ) );
            else
                result = await dispatcher.InvokeAsync( ( ) => action( GetMainWindow( ) ) );

            return result;
        }

        private static void UpdateMainWindow( Action<Window> action ) {
            var dispatcher = Application.Current.Dispatcher;
            if( dispatcher.CheckAccess( ) )
                action( GetMainWindow( ) );
            else
                dispatcher.BeginInvoke( ( ) => action( GetMainWindow( ) ) );
        }

        public override void FlushInputBuffer( ) => throw new NotImplementedException( );

        public override KeyInfo ReadKey( ReadKeyOptions options ) => throw new NotImplementedException( );

        public override BufferCell[,] GetBufferContents( Rectangle rectangle ) => throw new NotImplementedException( );
        public override void SetBufferContents( Rectangle rectangle, BufferCell fill ) => throw new NotImplementedException( );
        public override void SetBufferContents( Coordinates origin, BufferCell[,] contents ) => throw new NotImplementedException( );
        public override void ScrollBufferContents( Rectangle source, Coordinates destination, Rectangle clip, BufferCell fill ) => throw new NotImplementedException( );

        public WpfHostRawUserInterface QueueWrite( string value, ConsoleColor? foregroundColor = null, ConsoleColor? backgroundColor = null ) {
            var view = this.View;
            Action<string> write = view.Write;
            view.Dispatcher.BeginInvoke( DispatcherPriority.Normal, write, value );
            return this;
        }

        public WpfHostRawUserInterface QueueProgress( ProgressRecord record ) {
            var view = this.View;
            Action<ProgressRecord> progress = view.Progress;
            view.Dispatcher.BeginInvoke( DispatcherPriority.Normal, progress, record );
            return this;
        }
    }

}

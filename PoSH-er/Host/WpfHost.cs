﻿using System;
using System.Globalization;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Host;
using System.Management.Automation.Runspaces;
using System.Threading.Tasks;
using EmperialApps.PoSHer.Views;

namespace EmperialApps.PoSHer.Host {

    public class WpfHost : PSHost {
        private readonly WpfHostUserInterface _ui = new WpfHostUserInterface( );

        public ScriptView View {
            get => this._ui.View;
            private set => this._ui.View = value;
        }

        public override string Name => "PoSH-er WPF Host";
        public override PSHostUserInterface UI => this._ui;
        public override Guid InstanceId { get; } = Guid.NewGuid( );
        public override Version Version { get; } = new Version( 1, 0 );
        public override CultureInfo CurrentCulture { get; } = CultureInfo.CurrentCulture;
        public override CultureInfo CurrentUICulture { get; } = CultureInfo.CurrentUICulture;

        public async Task<PSDataCollection<PSObject>> RunScriptAsync( ScriptView view, Runspace runspace, bool redirect = true ) {
            this.View = view;

            var ps = PowerShell
                .Create( runspace )
                .AddScript( view.Script );

            if( redirect )
                ps.AddCommand( "Out-Default" );

            var errors = ps.Streams.Error;
            errors.DataAdded += ( s, e ) => this.OnErrorRecordAdded( (PSDataCollection<ErrorRecord>)s, e );

            var output = new PSDataCollection<PSObject>( );
            view.RunStarted( ps.Stop );
            try {
                await ps.InvokeAsync( default( PSDataCollection<PSObject> ), output );

                if( errors.Count > 0 )
                    throw new CompletedWithErrorsException( output, errors.Select( r => r.Exception ) );

                return output;
            }
            catch( CmdletInvocationException ex ) {
                this.WriteErrorRecord( ex.ErrorRecord );
                throw new CompletedWithErrorsException( output, ex );
            }
            finally {
                view.RunFinished( );
            }
        }

        private void OnErrorRecordAdded( PSDataCollection<ErrorRecord> errors, DataAddedEventArgs e ) => this.WriteErrorRecord( errors[e.Index] );
        private void WriteErrorRecord( ErrorRecord errorRecord ) {
            var invocationInfo = errorRecord.InvocationInfo;
            this._ui.WriteErrorLine(
                invocationInfo.InvocationName + ": " + errorRecord + Environment.NewLine
              + invocationInfo.PositionMessage );
        }

        public override void EnterNestedPrompt( ) => throw new NotImplementedException( );
        public override void ExitNestedPrompt( ) => throw new NotImplementedException( );
        public override void NotifyBeginApplication( ) => throw new NotImplementedException( );
        public override void NotifyEndApplication( ) => throw new NotImplementedException( );
        public override void SetShouldExit( int exitCode ) => throw new NotImplementedException( );
    }
}
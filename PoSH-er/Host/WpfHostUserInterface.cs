﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Management.Automation;
using System.Management.Automation.Host;
using System.Security;
using EmperialApps.PoSHer.Views;

namespace EmperialApps.PoSHer.Host {

    public class WpfHostUserInterface : PSHostUserInterface {
        private readonly WpfHostRawUserInterface _rawUI = new WpfHostRawUserInterface( );

        public override PSHostRawUserInterface RawUI => this._rawUI;

        public ScriptView View {
            get => this._rawUI.View;
            set => this._rawUI.View = value;
        }

        public override int PromptForChoice( string caption, string message, Collection<ChoiceDescription> choices, int defaultChoice ) {
            var view = this.View;
            var response = view.Dispatcher.Invoke( ( ) => view.PromptForChoiceAsync( caption, message, choices, defaultChoice ) );
            return response.Result;
        }

        public override Dictionary<string, PSObject> Prompt( string caption, string message, Collection<FieldDescription> descriptions ) => throw new NotImplementedException( );
        public override PSCredential PromptForCredential( string caption, string message, string userName, string targetName ) => throw new NotImplementedException( );
        public override PSCredential PromptForCredential( string caption, string message, string userName, string targetName, PSCredentialTypes allowedCredentialTypes, PSCredentialUIOptions options ) => throw new NotImplementedException( );

        public override string ReadLine( ) => throw new NotImplementedException( );
        public override SecureString ReadLineAsSecureString( ) => throw new NotImplementedException( );

        public override void Write( string value ) => this.QueueWrite( value );
        public override void Write( ConsoleColor foregroundColor, ConsoleColor backgroundColor, string value ) => this.QueueWrite( value, foregroundColor, backgroundColor );
        public override void WriteLine( string value ) => this.QueueWriteLine( value );

        public override void WriteProgress( long sourceId, ProgressRecord record ) => this._rawUI.QueueProgress( record );
        public override void WriteErrorLine( string value ) => this.QueueWriteLine( "[💣] ", value );
        public override void WriteWarningLine( string message ) => this.QueueWriteLine( "[⚠️] ", message );
        public override void WriteVerboseLine( string message ) => this.QueueWriteLine( "[🗣️] ", message );
        public override void WriteDebugLine( string message ) => this.QueueWriteLine( "[🐞] ", message );

        private void QueueWriteLine( string value, ConsoleColor? foregroundColor = null, ConsoleColor? backgroundColor = null )
             => this.QueueWrite( value, foregroundColor, backgroundColor )
                    .QueueWrite( Environment.NewLine, foregroundColor, backgroundColor );

        private void QueueWriteLine( string prefix, string value, ConsoleColor? foregroundColor = null, ConsoleColor? backgroundColor = null )
             => this.QueueWrite( prefix, foregroundColor, backgroundColor )
                    .QueueWrite( value, foregroundColor, backgroundColor )
                    .QueueWrite( Environment.NewLine, foregroundColor, backgroundColor );

        private WpfHostRawUserInterface QueueWrite( string value, ConsoleColor? foregroundColor = null, ConsoleColor? backgroundColor = null )
                         => this._rawUI.QueueWrite( value, foregroundColor, backgroundColor );
    }
}

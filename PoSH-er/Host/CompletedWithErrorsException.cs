﻿using System;
using System.Collections.Generic;
using System.Management.Automation;

namespace EmperialApps.PoSHer.Host {

    public class CompletedWithErrorsException : AggregateException {
        public CompletedWithErrorsException( PSDataCollection<PSObject> output, params Exception[] innerExceptions ) : base( innerExceptions ) => this.Output = output;
        public CompletedWithErrorsException( PSDataCollection<PSObject> output, IEnumerable<Exception> innerExceptions ) : base( innerExceptions ) => this.Output = output;

        public PSDataCollection<PSObject> Output { get; }
    }
}

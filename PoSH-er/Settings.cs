﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management.Automation.Runspaces;
using System.Threading.Tasks;
using EmperialApps.PoSHer.Views;
using EmperialApps.PoSHer.Host;
using Microsoft.PowerShell;

namespace EmperialApps.PoSHer {

    public class Settings {
        private static string _settingsDirectory;

        private readonly Dictionary<string, ScriptView> _views = new Dictionary<string, ScriptView>( );
        private readonly MainWindow _mainWindow;
        private readonly Runspace _runspace;
        private readonly WpfHost _host;

        public Settings( MainWindow mainWindow ) {
            this._mainWindow = mainWindow;
            this._host = new WpfHost( );
            this._runspace = RunspaceFactory.CreateRunspace( this._host );
            this._runspace.ThreadOptions = PSThreadOptions.UseCurrentThread;
            this._runspace.InitialSessionState.ExecutionPolicy = ExecutionPolicy.RemoteSigned;
            this._runspace.InitialSessionState.Variables.Add( new SessionStateVariableEntry( nameof( MainWindow ), mainWindow, "The main application window." ) );
            this._runspace.InitialSessionState.Variables.Add( new SessionStateVariableEntry( nameof( this.SettingsView ), this.SettingsView, "The application settings pane." ) );

            this._runspace.Open( );
            this._runspace.SessionStateProxy.Path.SetLocation( SettingsDirectory );
        }

        private static string Namespace => typeof( Settings ).Namespace;

        public static string SettingsDirectory => _settingsDirectory ??= GetSettingsDirectory( );

        private SettingsView SettingsView => this._mainWindow.SettingsView;

        private static string GetSettingsDirectory( ) {
            string baseDirectory = Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData );
            string settingsSubdirectory = Namespace.Replace( '.', Path.DirectorySeparatorChar ).Replace( "PoSHer", "PoSH-er" );
            string settingsDirectory = Path.Join( baseDirectory, settingsSubdirectory );
            if( !Directory.Exists( settingsDirectory ) )
                Directory.CreateDirectory( settingsDirectory );

            return settingsDirectory;
        }

        public Task LoadAsync( ) => this.RunScriptAsync( "LoadSettings.ps1" );
        public Task SaveAsync( ) => this.RunScriptAsync( "SaveSettings.ps1" );
        public Task LoadActionsAsync( ) => this.RunScriptAsync( "LoadActions.ps1" );
        public Task SaveActionsAsync( ) => this.RunScriptAsync( "SaveActions.ps1" );
        public Task AutoSaveActionsAsync( ) => this.SettingsView.AutoSave ? this.SaveActionsAsync( ) : Task.CompletedTask;

        private async Task RunScriptAsync( string scriptName ) {
            if( !this._views.TryGetValue( scriptName, out var view ) ) {
                this._views[scriptName] = view = new ScriptView( "./" + scriptName );
                this.SettingsView.AddSettingScript( view );
                view.BringIntoView( );
            }

            // Ensure scripts are up to date.
            var assembly = typeof( Settings ).Assembly;
            string settingsDirectory = SettingsDirectory;
            DateTime assemblyWriteTime = File.GetLastWriteTimeUtc( assembly.Location );
            DateTime settingsWriteTime = File.GetLastWriteTimeUtc( Path.Join( settingsDirectory, scriptName ) );
            if( assemblyWriteTime > settingsWriteTime ) {
                view.Write( "Updating scripts..." );
                view.Write( Environment.NewLine );
                Directory.CreateDirectory( settingsDirectory );

                var scriptsPrefix = Namespace + ".Scripts.";
                var scriptNames = assembly.GetManifestResourceNames( ).Where( s => s.StartsWith( scriptsPrefix ) );
                foreach( string name in scriptNames ) {
                    string fileName = name.Substring( scriptsPrefix.Length );
                    string filePath = Path.Join( settingsDirectory, fileName );
                    view.Write( " - " );
                    view.Write( fileName );
                    view.Write( Environment.NewLine );

                    await using var fileStream = File.Create( filePath );
                    await using var resourceStream = assembly.GetManifestResourceStream( name ); // ReSharper disable once PossibleNullReferenceException
                    await resourceStream.CopyToAsync( fileStream );
                }
            }

            var output = await this._host.RunScriptAsync( view, this._runspace, redirect: false );
        }
    }
}

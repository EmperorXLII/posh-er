﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace EmperialApps.PoSHer.Views {

    /// <summary>
    /// Vertical stack panel that sizes children to at most the parent's viewport size.
    /// </summary>
    /// <remarks>
    /// <see cref="Panel.Tag"/> must be bound to the parent <see cref="ScrollContentPresenter"/>.
    /// </remarks>
    public class ViewportVerticalStackPanel : Panel {
        protected override Size MeasureOverride( Size constraint ) {
            Size viewport = ItemsView.GetViewportSize( this );
            Size availableSize = new Size( constraint.Width, viewport.Height );

            Size panelDesiredSize = default;
            foreach( UIElement child in this.InternalChildren ) {
                child.Measure( availableSize );

                Size childDesiredSize = child.DesiredSize;
                panelDesiredSize.Width = Math.Max( panelDesiredSize.Width, childDesiredSize.Width );
                panelDesiredSize.Height += childDesiredSize.Height;
            }

            return panelDesiredSize;
        }

        protected override Size ArrangeOverride( Size finalSize ) {
            Size viewport = ItemsView.GetViewportSize( this );

            Rect childRect = new Rect( finalSize );
            foreach( UIElement child in this.InternalChildren ) {
                childRect.Height = Math.Min( child.DesiredSize.Height, viewport.Height );

                child.Arrange( childRect );

                childRect.Y += childRect.Height;
            }

            return finalSize;
        }
    }
}

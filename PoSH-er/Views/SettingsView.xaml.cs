﻿using System.Windows.Controls;

namespace EmperialApps.PoSHer.Views {

    public partial class SettingsView : UserControl {
        public SettingsView( ) => this.InitializeComponent( );

        #region Persisted Settings // ReSharper disable UnusedMember.Global

        public bool ShowSettingsOutput {
            get => this._showSettingsOutput.IsChecked.GetValueOrDefault( );
            set => this._showSettingsOutput.IsChecked = value;
        }

        public bool AutoSave {
            get => this._autoSave.IsChecked.GetValueOrDefault( );
            set => this._autoSave.IsChecked = value;
        }

        #endregion // ReSharper restore UnusedMember.Global

        public void AddSettingScript( ScriptView view ) {
            view.VerticalScrollBarVisibility = ScrollBarVisibility.Hidden;
            this._settingsScripts.Items.Add( view );
        }
    }
}

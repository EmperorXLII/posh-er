﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using System.Windows.Controls;

namespace EmperialApps.PoSHer.Views {

    public partial class ProgressView : UserControl {
        public ProgressView( int activityId ) {
            this.InitializeComponent( );

            this.ActivityId = activityId;
        }

        public int ActivityId { get; }

        public void Update( ProgressRecord record ) {
            this._bar.Value = record.PercentComplete;

            var progressText = new[] { record.Activity, record.CurrentOperation, record.StatusDescription }.SelectMany( GetText );
            this._text.Text = string.Join( Environment.NewLine, progressText );
        }

        private static IEnumerable<string> GetText( string status ) {
            string[] parts = status?.Split( "        " ) ?? Array.Empty<string>( );
            foreach( string part in parts ) {
                if( !string.IsNullOrWhiteSpace( part ) )
                    yield return part.Trim( );
            }
        }
    }

}

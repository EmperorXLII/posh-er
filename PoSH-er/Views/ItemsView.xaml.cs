﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace EmperialApps.PoSHer.Views {

    public partial class ItemsView : UserControl {
        public ItemsView( ) {
            this.InitializeComponent( );

            this.EnableNestedScrolling( (ScrollViewer)this.Content );
        }

        public ItemCollection Items => this._container.Items;

        public static readonly DependencyProperty ViewportSizeProperty =
            DependencyProperty.RegisterAttached(
                "ViewportSize",
                typeof( Size ),
                typeof( ItemsView ),
                new FrameworkPropertyMetadata( new Size( double.PositiveInfinity, double.PositiveInfinity ), FrameworkPropertyMetadataOptions.Inherits )
            );

        public static Size GetViewportSize( DependencyObject d ) => (Size)d.GetValue( ViewportSizeProperty );
        public static void SetViewportSize( DependencyObject d, Size value ) => d.SetValue( ViewportSizeProperty, value );

        protected override Size MeasureOverride( Size constraint ) {
            SetViewportSize( this, constraint );
            Size desiredSize = base.MeasureOverride( constraint );
            return desiredSize;
        }

        #region Nested Scrolling

        private Vector _previousScroll;

        private void EnableNestedScrolling( ScrollViewer scroll ) {
            scroll.ScrollChanged += this.OnScrollChanged;
            scroll.AddHandler( MouseWheelEvent, new MouseWheelEventHandler( this.OnMouseWheel ), handledEventsToo: true );
        }

        private void OnScrollChanged( object sender, ScrollChangedEventArgs e )
            => this._previousScroll = new Vector( e.HorizontalChange, e.VerticalChange );

        private void OnMouseWheel( object sender, MouseWheelEventArgs e ) {
            if( !e.Handled || this._previousScroll.LengthSquared > 0.0 )
                this._previousScroll = default;
            else
                e.Handled = false; // (allow parent to handle mouse wheel, if scroll viewer "handling" did not change offset)
        }

        #endregion
    }
}

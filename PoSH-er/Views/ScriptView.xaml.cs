﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Host;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace EmperialApps.PoSHer.Views {

    public partial class ScriptView : UserControl {
        private readonly StringBuilder _pending = new StringBuilder( );
        private DispatcherOperation _lastShowOperation;
        private Action _cancel;

        public ScriptView( string script ) {
            this.InitializeComponent( );

            this._expander.Header = script;
        }

        public string Script => (string)this._expander.Header;

        public ScrollBarVisibility VerticalScrollBarVisibility {
            get => this._output.VerticalScrollBarVisibility;
            set => this._output.VerticalScrollBarVisibility = value;
        }

        public void RunStarted( Action cancelCallback ) {
            this._cancel = cancelCallback;
            CommandManager.InvalidateRequerySuggested( );
        }

        public void RunFinished( ) {
            this._cancel = null;
            CommandManager.InvalidateRequerySuggested( );
        }

        public void Write( string value ) {
            if( string.IsNullOrEmpty( this._output.Text ) )
                value = value.TrimStart( );

            this._pending.Append( value );

            if( string.IsNullOrWhiteSpace( value ) )
                return;

            this._output.Text += this._pending;
            this._pending.Clear( );

            if( this._lastShowOperation?.Status != DispatcherOperationStatus.Pending )
                this._lastShowOperation = this.Dispatcher.BeginInvoke( this.ShowLatestWrite, DispatcherPriority.Render );
        }

        private void WriteLine( string value = null ) {
            if( value != null )
                this.Write( value );
            this.Write( Environment.NewLine );
        }

        private void ShowLatestWrite( ) {
            this.BringIntoView( new Rect( 0, this.ActualHeight, 1, 1 ) );
            this._output.ScrollToEnd( );
        }

        public void Progress( ProgressRecord record ) {
            var progress = this._progressContainer.Children.Cast<ProgressView>( ).FirstOrDefault( p => p.ActivityId == record.ActivityId );
            if( progress == null ) {
                progress = new ProgressView( record.ActivityId );
                this._progressContainer.Children.Add( progress );
            }

            progress.Update( record );

            if( record.RecordType == ProgressRecordType.Completed )
                this._progressContainer.Children.Remove( progress );
        }

        private void CanCancel( object sender, CanExecuteRoutedEventArgs e )
            => e.CanExecute = this._cancel != null;
        private void ExecuteCancel( object sender, ExecutedRoutedEventArgs e ) {
            this._cancel.Invoke( );
            this.WriteLine( );
            this.WriteLine( "<canceled>" );
        }

        #region PromptForChoice

        private TaskCompletionSource<int> _response;

        private static bool IsSupported( ChoiceDescription choice ) => choice.Label != "&Suspend";

        public Task<int> PromptForChoiceAsync( string caption, string message, Collection<ChoiceDescription> choices, int defaultChoice ) {
            this._response = new TaskCompletionSource<int>( );

            string prompt = caption.TrimEnd( ':' ) + ": " + message;
            this.WriteLine( );
            this.Write( "[🙋] " );
            this.WriteLine( prompt );
            this._prompt.Text = prompt;
            this._prompt.Visibility = Visibility.Visible;

            foreach( ChoiceDescription choice in choices )
                this._choices.Children.Add( new Button {
                    Content = choice.Label.Replace( '&', '_' ),
                    ToolTip = choice.HelpMessage,
                    IsEnabled = IsSupported( choice )
                } );
            this._choices.Children[defaultChoice].Focus( );

            return this._response.Task;
        }

        private void OnOptionClicked( object sender, RoutedEventArgs e ) {
            var choice = (Button)e.OriginalSource;
            this.WriteLine( ((string)choice.Content).Replace( "_", "" ) );
            int index = this._choices.Children.IndexOf( choice );
            this._response.SetResult( index );

            this._response = null;
            this._choices.Children.Clear( );
            this._prompt.Visibility = Visibility.Collapsed;
        }

        #endregion
    }
}

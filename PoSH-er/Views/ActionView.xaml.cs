﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace EmperialApps.PoSHer.Views {

    public partial class ActionView : UserControl {
        private static readonly char[] IndexCharacters = { '[', '_', ']' };

        public ActionView( string name, string script ) {
            this.InitializeComponent( );

            this._script.AddHandler( KeyDownEvent, new KeyEventHandler( this.RestoreDeleteShortcut ), handledEventsToo: true );
            this.ActionCommandExecuted += delegate { this._script.Focus( ); };

            this.Script = script;
            this.EndRename( name );
            this.UpdateScriptHighlight( );
        }

        public string ActionName {
            get => (string)this._name.Content;
            set => this._name.Content = value;
        }

        public string Script {
            get => this._script?.Text ?? "";
            set => this._script.Text = value;
        }

        public event ExecutedRoutedEventHandler ActionCommandExecuted;

        private static bool IsIndexName( string name )
            => ushort.TryParse( name.Trim( IndexCharacters ), out ushort index )
            && index < 100;

        private void OnNameClicked( object sender, RoutedEventArgs e ) {
            if( this.IsKeyboardFocusWithin )
                ApplicationCommands.Open.Execute( parameter: null, target: this );

            this._script.Focus( );
        }

        private void CanExecuteScript( object sender, CanExecuteRoutedEventArgs e )
            => e.CanExecute = !string.IsNullOrWhiteSpace( this.Script );
        private void ExecuteActionCommand( object sender, ExecutedRoutedEventArgs e )
            => this.ActionCommandExecuted?.Invoke( this, e );

        private void OnScriptFocusChanged( object sender, RoutedEventArgs e ) => this.UpdateScriptHighlight( );
        private void UpdateScriptHighlight( ) =>
            this._script.Background =
                this._script.IsFocused
                    ? SystemColors.WindowBrush
                    : SystemColors.ControlBrush;

        private void RestoreDeleteShortcut( object sender, KeyEventArgs e ) {
            if( e.Handled && Commands.DeleteAction.InputGestures[0].Matches( this, e ) )
                e.Handled = false;
        }

        #region Rename

        private void ExecuteRename( object sender, ExecutedRoutedEventArgs e ) => this.BeginRename( this.ActionName );

        private void OnRenameLostFocus( object sender, RoutedEventArgs e ) => this.EndRename( this._rename.Text );
        private void OnRenameKeyDown( object sender, KeyEventArgs e ) {
            switch( e.Key ) {
                case Key.Enter:
                    this.EndRename( this._rename.Text );
                    break;
                case Key.Escape:
                    this.EndRename( null );
                    break;
            }
        }

        private void BeginRename( string name ) {
            this._rename.Tag = this._rename.Text = name;
            this._rename.Visibility = Visibility.Visible;
            this._rename.SelectAll( );
            this._rename.Focus( );
            this.UpdateNameLayout( isShort: false );
        }

        private void EndRename( string name ) {
            this._rename.Visibility = Visibility.Collapsed;

            if( string.IsNullOrWhiteSpace( name ) ) {
                string originalName = (string)this._rename.Tag;
                this._rename.Text = name = originalName;
            }

            this.ActionName = name;

            bool isShort = name.Length < 3 || IsIndexName( name );
            this.UpdateNameLayout( isShort );
            this._script.Focus( );
        }

        private void UpdateNameLayout( bool isShort ) {
            this._name.HorizontalAlignment = isShort ? HorizontalAlignment.Center : HorizontalAlignment.Stretch;
            Grid.SetColumnSpan( (Button)this._name.Parent, isShort ? 1 : 2 );
            Grid.SetRow( this._script, isShort ? 0 : 1 );
        }

        #endregion
    }
}

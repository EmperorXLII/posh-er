﻿using System.Management.Automation.Runspaces;
using System.Windows.Controls;
using System.Windows.Media;

namespace EmperialApps.PoSHer.Views {

    public partial class SequenceView : UserControl {
        public SequenceView( ) => this.InitializeComponent( );

        public int Count => this._container.Items.Count;
        public ScriptView this[int index] => (ScriptView)this._container.Items[index];
        public void Add( ScriptView view ) => this._container.Items.Add( view );

        public void SetState( PipelineState state ) {
            if( state != PipelineState.Running )
                this._border.Opacity = 0.75;

            this._border.BorderBrush = state switch
            {
                PipelineState.Completed => Brushes.Green,
                PipelineState.Disconnected => Brushes.Gray,
                PipelineState.Stopped => Brushes.Gray,
                PipelineState.Failed => Brushes.Red,
                PipelineState.Running => Brushes.Blue,
                _ => this._border.BorderBrush
            };
        }
    }
}

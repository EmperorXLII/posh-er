﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using EmperialApps.PoSHer.Host;
using EmperialApps.PoSHer.Views;
using Microsoft.PowerShell;

namespace EmperialApps.PoSHer {

    public partial class MainWindow : Window {
        private readonly Dictionary<ICommand, Action<ActionView>> _actionCommandHandlers;
        private readonly Runspace _runspace;
        private readonly WpfHost _host;
        private SequenceView _sequence;

        public MainWindow( ) {
            this.InitializeComponent( );

            this._actionCommandHandlers = new Dictionary<ICommand, Action<ActionView>> {
                [Commands.AddAction] = this.OnAddActionToSequence,
                [Commands.RunAction] = this.OnRunActionInSequence,
                [Commands.DeleteAction] = this.OnDeleteAction,
            };

            this._host = new WpfHost( );
            this._runspace = RunspaceFactory.CreateRunspace( this._host );
            this._runspace.InitialSessionState.ExecutionPolicy = ExecutionPolicy.RemoteSigned;
            this._runspace.InitialSessionState.Variables.Add( new SessionStateVariableEntry( "ErrorActionPreference", ActionPreference.Inquire, "The error action preference." ) );
            this._runspace.InitialSessionState.Variables.Add( new SessionStateVariableEntry( nameof( Settings.SettingsDirectory ), Settings.SettingsDirectory, "The settings directory." ) );
            this._runspace.Open( );
        }

        #region Additional Persisted Settings // ReSharper disable UnusedMember.Global

        public IEnumerable<ActionView> Actions => this._actions.Items.Cast<ActionView>( );

        public bool ShowLoading {
            get => this._loading.Visibility == Visibility.Visible;
            set => this._loading.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
        }

        public bool ShowSettings {
            get => this._showSettingsMenuItem.IsChecked;
            set => this._showSettingsMenuItem.IsChecked = value;
        }

        public double ActionsRowHeight {
            get => this._actionsRow.Height.Value;
            set => this._actionsRow.Height = new GridLength( pixels: value );
        }

        #endregion // ReSharper restore UnusedMember.Global

        public event EventHandler Ran;

        public void AddAction( string name, string script ) {
            var action = new ActionView( name, script );
            this._actions.Items.Add( action );
            action.ActionCommandExecuted += this.OnActionCommand;

            action.MoveFocus( new TraversalRequest( FocusNavigationDirection.Down ) );
        }

        private void OnDeleteAction( ActionView action ) {
            var except = new[] { action };
            var next = this.Actions.SkipUntil( action ).Except( except ).FirstOrDefault( )
                    ?? this.Actions.TakeUntil( action ).Except( except ).LastOrDefault( );
            next?.MoveFocus( new TraversalRequest( FocusNavigationDirection.Down ) );

            action.ActionCommandExecuted -= this.OnActionCommand;
            this._actions.Items.Remove( action );
        }

        private void OnActionCommand( object sender, ExecutedRoutedEventArgs e ) {
            try {
                var action = (ActionView)sender;
                var handler = this._actionCommandHandlers[e.Command];
                handler( action );
            }
            catch( Exception ex ) {
                App.ShowException( this, ex );
            }
        }

        private void OnAddActionToSequence( ActionView action ) {
            if( this._sequence == null ) {
                this._sequence = new SequenceView( );
                this._sequences.Items.Add( this._sequence );
                this._runButton.IsEnabled = true;
            }

            var view = new ScriptView( action.Script );
            this._sequence.Add( view );
            view.BringIntoView( );
        }

        private void OnRunClicked( object sender, EventArgs e ) => this.OnRunActionInSequence( null );
        private async void OnRunActionInSequence( ActionView action ) {
            if( action != null && this._sequence?[^1]?.Script != action.Script )
                this.OnAddActionToSequence( action );

            if( !this._runButton.IsEnabled )
                return;

            this._runButton.IsEnabled = false;

            if( this._sequence == null )
                return;

            this._sequence.SetState( PipelineState.Running );
            try {
                for( int i = 0; i < this._sequence.Count; ++i ) {
                    var script = this._sequence[i];
                    var output = await this._host.RunScriptAsync( script, this._runspace );
                    if( output.Count > 0 ) {
                        string result = string.Join( Environment.NewLine, output );
                        MessageBox.Show( "Missed output:" + Environment.NewLine + result );
                    }
                }

                this._sequence.SetState( PipelineState.Completed );
                this.Ran?.Invoke( this, EventArgs.Empty );
            }
            catch( RuntimeException ex ) when( ex is PipelineStoppedException
                                            || ex is ActionPreferenceStopException ) {
                this._sequence.SetState( PipelineState.Stopped );
            }
            catch( CompletedWithErrorsException ) {
                this._sequence.SetState( PipelineState.Failed );
            }
            catch( Exception ex ) {
                this._sequence.SetState( PipelineState.Failed );
                App.ShowException( this, ex );
            }
            finally {
                this._sequence = null;
            }
        }

        private void ToggleMenuItem( object sender, RoutedEventArgs e ) {
            var menuItem = (MenuItem)sender;
            menuItem.IsChecked = !menuItem.IsChecked;
        }

        private void ExecuteNewAction( object sender, RoutedEventArgs e ) {
            // De-bounce multiple 'Ctrl+N's by returning focus to any previously created empty script.
            var lastAction = this.Actions.LastOrDefault( );
            if( lastAction != null && string.IsNullOrWhiteSpace( lastAction.Script ) ) {
                lastAction.Focus( );
                return;
            }

            // Give the new script a unique name.
            var usedNames = this.Actions.Select( a => a.ActionName );
            var indexNames = Enumerable.Range( 0, ushort.MaxValue ).Select( i => "[" + i.ToString( ).PadLeft( 2, '_' ) + "]" );
            string uniqueName = indexNames.Except( usedNames ).First( );
            this.AddAction( uniqueName, "" );
        }

        private async void ExecuteSaveActions( object sender, RoutedEventArgs e ) => await App.Current.SaveActionsAsync( );

        private void ExecuteExit( object sender, RoutedEventArgs e ) => this.Close( );

        private void OnActionsMenuOpened( object sender, RoutedEventArgs e ) {
            if( e.Source != sender )
                return;

            var actionsMenu = (MenuItem)sender;
            actionsMenu.Items.Clear( );

            var items = this.CreateMenuItemsFromBoundCommands( actionsMenu.Items );

            items.Add( new Separator( ) );
            foreach( var action in this.Actions ) {
                items.Add( new MenuItem {
                    Header = action.ActionName,
                    ItemsSource = action.CreateMenuItemsFromBoundCommands( )
                } );
            }
        }

        private void OnRightClick( object sender, MouseButtonEventArgs e ) {
            var source = (e.OriginalSource as DependencyObject).GetAncestorsInclusive<UIElement>( ).FirstOrDefault( );
            if( source is null )
                return;

            var menu = new ContextMenu { PlacementTarget = source };

            var immediateAncestors = source.GetAncestorsInclusive<UIElement>( ).TakeUntil( a => a is ItemsControl || a is Window );
            foreach( UIElement ancestor in immediateAncestors )
                ancestor.CreateMenuItemsFromBoundCommands( menu.Items );

            menu.IsOpen = menu.Items.Count > 0;
        }
    }
}

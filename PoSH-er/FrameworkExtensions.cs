﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace EmperialApps.PoSHer {

    public static class FrameworkExtensions {
        public static IEnumerable<TResult> Select<TResult>( this CommandBindingCollection commandBindings, Func<CommandBinding, TResult> selector )
            => commandBindings.Cast<CommandBinding>( ).Select( selector );

        public static IEnumerable<TSource> TakeUntil<TSource>( this IEnumerable<TSource> source, Func<TSource, bool> predicate ) => source.TakeWhile( item => !predicate( item ) );
        public static IEnumerable<TSource> SkipUntil<TSource>( this IEnumerable<TSource> source, Func<TSource, bool> predicate ) => source.SkipWhile( item => !predicate( item ) );
        public static IEnumerable<TSource> TakeUntil<TSource>( this IEnumerable<TSource> source, TSource target ) where TSource : class => source.TakeUntil( item => item == target );
        public static IEnumerable<TSource> SkipUntil<TSource>( this IEnumerable<TSource> source, TSource target ) where TSource : class => source.SkipUntil( item => item == target );

        public static DependencyObject GetParent( DependencyObject ancestor )
            => ancestor is Visual
             ? VisualTreeHelper.GetParent( ancestor )
             : LogicalTreeHelper.GetParent( ancestor );

        public static IEnumerable<T> GetAncestorsInclusive<T>( this DependencyObject child )
            where T : DependencyObject {
            for( var ancestor = child; ancestor != null; ancestor = GetParent( ancestor ) ) {
                if( ancestor is T target )
                    yield return target;
            }
        }

        public static IList CreateMenuItemsFromBoundCommands( this UIElement target, IList items = null, bool separate = true ) {
            items ??= new List<object>( );

            foreach( CommandBinding binding in target.CommandBindings ) {
                if( separate ) {
                    separate = false;
                    if( items.Count > 0 )
                        items.Add( new Separator( ) );
                }

                items.Add( new MenuItem { Command = binding.Command, CommandTarget = target } );
            }

            return items;
        }
    }
}

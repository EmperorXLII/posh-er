﻿using System;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace EmperialApps.PoSHer {

    public static class Commands {
        private static readonly KeyGestureConverter _gestureConverter = new KeyGestureConverter( );
        private static InputGestureCollection Gestures( params string[] shortcuts )
                => new InputGestureCollection( Array.ConvertAll( shortcuts, _gestureConverter.ConvertFromInvariantString ) );

        private static RoutedUICommand CreateCommand( string text, InputGestureCollection gestures, [CallerMemberName] string name = null )
                => new RoutedUICommand2( text, name ?? throw new ArgumentNullException( nameof( name ) ), typeof( Commands ), gestures );

        public static RoutedUICommand NewAction { get; } = CreateCommand( "_New Action", Gestures( "Ctrl+N" ) );
        public static RoutedUICommand SaveActions { get; } = CreateCommand( "_Save Actions", Gestures( "Ctrl+S" ) );

        public static RoutedUICommand AddAction { get; } = CreateCommand( "_Add Action", Gestures( "Ctrl+Add", "Ctrl+Plus" ) );
        public static RoutedUICommand RunAction { get; } = CreateCommand( "_Run Action", Gestures( "F5", "Ctrl+R" ) );
        public static RoutedUICommand RenameAction { get; } = CreateCommand( "Rena_me Action", Gestures( "F2" ) );
        public static RoutedUICommand DeleteAction { get; } = CreateCommand( "_Delete Action", Gestures( "Ctrl+Delete" ) );

        public static RoutedUICommand CancelScript { get; } = CreateCommand( "_Cancel Script", Gestures( "Ctrl+Cancel", "Ctrl+C" ) );

        public static RoutedUICommand Exit { get; } = CreateCommand( "E_xit", Gestures( "Alt+F4" ) );

        private class RoutedUICommand2 : RoutedUICommand {
            public RoutedUICommand2( string text, string name, Type ownerType, InputGestureCollection inputGestures ) : base( text, name, ownerType, inputGestures ) { }
            public override string ToString( ) => nameof( RoutedUICommand ) + ": " + this.Name;
        }
    }
}

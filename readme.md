# PoSH-er: PowerShell Sequencer

Super shell sequencing support: dynamically build and save sequences of commands, even while existing ones continue to execute!
